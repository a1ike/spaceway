$(document).ready(function () {

  if ($(window).width() < 1024) {
    $('.sw-top').addClass('sw-top_static');
  } else {
    $('.sw-top').removeClass('sw-top_static');
  }

  $('#sw-yt').YTPlayer({
    fitToBackground: true,
    videoId: 'tGij4rCdyjQ',
    playerVars: {
      modestbranding: 0,
      autoplay: 1,
      controls: 0,
      showinfo: 0,
      branding: 0,
      rel: 0,
      autohide: 0
    }
  });

  $(window).resize(function () {

    if ($(window).width() < 1024) {
      $('.sw-top').addClass('sw-top_static');
    } else {
      $('.sw-top').removeClass('sw-top_static');
    }

  });

  $('#form').submit(function (e) {
    e.preventDefault();
    var f = $(this);
    $('.ierror', f).removeClass('ierror');
    var name = $('input[name="name"]', f).val();
    var phone = $('input[name="phone"]', f).val();
    var error = false;
    if (name == '') {
      $('input[name="name"]', f).addClass('ierror');
      error = true;
    }
    if (phone == '') {
      $('input[name="phone"]', f).addClass('ierror');
      error = true;
    }
    if (error) {
      return false;
    }
    $.ajax({
      type: 'POST',
      url: 'mail.php',
      data: $(this).serialize(),
    }).done(function (data) {
      if (data.response == 'ok') {
        $('.sw-thx').slideToggle('fast', function () {
          // Animation complete.
        });
        setTimeout(function () {
          $('.sw-thx').slideToggle('fast', function () {
            // Animation complete.
          });
        }, 2000);
      }
      else {
        alert('Ошибка! Заявка не отправлена, повторите запрос позже');
      }
    });
    return false;
  });

});